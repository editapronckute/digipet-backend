package com.digipet.backend.security;

import java.util.Optional;

import com.digipet.backend.models.User;
import com.digipet.backend.repositories.UserRepo;
import com.digipet.backend.models.DPUserDetails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class DPUserDetailsService implements UserDetailsService {

@Autowired
UserRepo userRepo;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<User> user = userRepo.findByUsername(username);
				user.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
				User u = user.get(); // to retrieve the user from optional

		return DPUserDetails.build(u);
	}

}