package com.digipet.backend.repositories;

import java.util.Optional;

import com.digipet.backend.models.ERole;
import com.digipet.backend.models.Role;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepo extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}
