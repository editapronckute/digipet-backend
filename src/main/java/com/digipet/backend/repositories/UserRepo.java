package com.digipet.backend.repositories;

import java.util.Optional;

import com.digipet.backend.models.User;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends CrudRepository<User, Long> {

  Optional<User> findByUsername (String username);
}
