package com.digipet.backend.models;

public enum ERole {
	ROLE_USER,
    ROLE_ADMIN
}
