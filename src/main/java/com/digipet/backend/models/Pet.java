package com.digipet.backend.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "PETS")
public class Pet implements Action {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @ManyToOne
    @JoinColumn(name = "owner_id")
    private User owner;
    private String name;
    @ElementCollection //creates seperate table
    @Enumerated(EnumType.STRING) // reads the string value
    private List<Mood> mood = new ArrayList<Mood>();
    State state;
    private int age;
    private Long lastFed;
    private Long lastPet;
    private Long lastBathed;
    private Long lastHappy;
    int timesFed;
    int timesPet;
    int timesBathed;

    public Pet(String name, User owner) {
        this.name = name;
        this.mood.add(Mood.MISERABLE);
        this.state = State.ALIVE;
        this.owner = owner;
        this.age = 0;
        this.timesBathed =0;
        this.timesFed = 0;
        this.timesPet = 0;
        this.lastHappy = System.currentTimeMillis();
    }
// used for testing
    public Pet(String name) {
        this.name = name;
        this.mood.add(Mood.MISERABLE);
        this.state = State.ALIVE;
        this.owner = null;
        this.age = 0;
        this.timesBathed =0;
        this.timesFed = 0;
        this.timesPet = 0;
        this.lastHappy = System.currentTimeMillis();
    }

    public Pet() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @JsonBackReference
    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public void careChecker() {
       // 1min = 60000 millisec
        if (mood.contains(Mood.MISERABLE)){
            mood.add(Mood.DIRTY);
            lastBathed = System.currentTimeMillis();
            mood.add(Mood.SAD); 
            lastPet = System.currentTimeMillis();
            mood.add(Mood.HUNGRY);
            lastFed = System.currentTimeMillis();
        }
        else if (mood.contains(Mood.DIRTY) && mood.contains(Mood.SAD) && mood.contains(Mood.HUNGRY) && !mood.contains(Mood.MISERABLE)){
            mood.add(Mood.MISERABLE);
            lastHappy = System.currentTimeMillis();
        }
    }

    public void ageChecker(){
        // every 3 times the pet gets bathed it will age
        if (timesBathed % 3 == 0)
        {
            age++;
        }
    }

    public void backToHappy() {
        if (!mood.contains(Mood.HUNGRY) && !mood.contains(Mood.DIRTY) && !mood.contains(Mood.SAD)) {
            mood.add(Mood.HAPPY);
        }
    }

    @Override
    public void feed() {
        if (mood.contains(Mood.HUNGRY)) {
            timesFed++;
            mood.remove(Mood.HUNGRY);
            lastFed = System.currentTimeMillis();
            backToHappy();
        }
    }

    @Override
    public void snuggle() {
        if (mood.contains(Mood.SAD)) {
            timesPet++;
            mood.remove(Mood.SAD);
            lastPet = System.currentTimeMillis();
            backToHappy();
        }
    }

    @Override
    public void bathe() {
        if (mood.contains(Mood.DIRTY)) {
            timesBathed++;
            mood.remove(Mood.DIRTY);
            lastBathed = System.currentTimeMillis();
            backToHappy();
        }
    }

    public void moodChanger() {
        long timeNow = System.currentTimeMillis();
        if (timeNow - lastFed > 120000) {
            mood.add(Mood.HUNGRY);
            mood.remove(Mood.HAPPY);
        } else if (timeNow - lastBathed > 180000) {
            mood.add(Mood.DIRTY);
            mood.remove(Mood.HAPPY);
        } else if (timeNow - lastPet > 60000) {
            mood.add(Mood.SAD);
            mood.remove(Mood.HAPPY);
        } else if (timeNow - lastHappy > 240000) {
            state = State.DEAD;
        }
    }

    @Override
    public String toString() {
        return "Pet [age=" + age + ", name=" + name + ", owner=" + owner + "]";
    }

    public long getId() {
        return id;
    }

    public List<Mood> getMood() {
        return mood;
    }

    public void setMood(List<Mood> mood) {
        this.mood = mood;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
	public void setId(long l) {
        this.id = l;
	}

    public Long getLastFed() {
        return lastFed;
    }

    public void setLastFed(Long lastFed) {
        this.lastFed = lastFed;
    }

    public Long getLastPet() {
        return lastPet;
    }

    public void setLastPet(Long lastPet) {
        this.lastPet = lastPet;
    }

    public Long getLastBathed() {
        return lastBathed;
    }

    public void setLastBathed(Long lastBathed) {
        this.lastBathed = lastBathed;
    }

    public Long getLastHappy() {
        return lastHappy;
    }

    public void setLastHappy(Long lastHappy) {
        this.lastHappy = lastHappy;
    }
}
