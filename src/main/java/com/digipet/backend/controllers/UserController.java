package com.digipet.backend.controllers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.digipet.backend.repositories.RoleRepo;
import com.digipet.backend.repositories.UserRepo;
import com.digipet.backend.models.ERole;
import com.digipet.backend.models.Role;
import com.digipet.backend.models.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;


@RestController
@CrossOrigin("http://localhost:8080/")
public class UserController {
    
    @Autowired
    UserRepo userRepo;

    @Autowired
    RoleRepo roleRepo;

    @Autowired  
    private PasswordEncoder passwordEncoder;

    //get all users
    @GetMapping("/users")
    public List<User> userlist(){
        return (List<User>) userRepo.findAll();
    }

    //get a user by id
    @GetMapping("/users/{id}")
    public User getById(@PathVariable Long id) {
        return userRepo.findById(id).orElseThrow(() -> new UserNotFoundException(id));
    }

    //get a user by username
    @GetMapping("/login/{username}")
    public User login(@PathVariable String username) {
        return userRepo.findByUsername(username)    
        .orElseThrow(() -> new UserNotFoundException(username));
    }

    //create a user
    @PostMapping("/signup")
    public User signUp(@RequestBody User newUser){
        Role newRole = new Role(ERole.ROLE_USER);
        Set<Role> roles = new HashSet<>();
        roles.add(newRole);
        newUser.setRoles(roles);
        roleRepo.save(newRole);
        newUser.setPassword(passwordEncoder.encode(newUser.getPassword())); // to get the passw and replace it with the encoded one
         return userRepo.save(newUser);
    }

    //update user details
    @PutMapping(value="users/{id}")
    public User replaceUser(@RequestBody User newUser, @PathVariable Long id){ 
        return userRepo.findById(id).map(User -> {
            if (newUser.getPassword() != null){ // null or empty // if pasword empty then ignore to avoid overriding it
                User.setPassword(passwordEncoder.encode(newUser.getPassword()));
            }
            if (!newUser.getEmail().isEmpty()){
                User.setEmail(newUser.getEmail());
            }
        return userRepo.save(User);
    })
    .orElseThrow(() -> new UserNotFoundException(id));
    }
    
    //remove user
    @DeleteMapping("/users/{id}")
    public void deleteUser(@PathVariable Long id) {
        userRepo.deleteById(id);
    }
}
