package com.digipet.backend.controllers;
 
public class UserNotFoundException extends RuntimeException{
    
    private static final long serialVersionUID = 1L;
 
    UserNotFoundException(Long id) {
        super("Could not find User " + id);
      }

    UserNotFoundException(String username) {
      super("Could not find the user" + username);
    }
}