package com.digipet.backend.unitTests;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import com.digipet.backend.models.Mood;
import com.digipet.backend.models.Pet;
import com.digipet.backend.models.State;

import org.junit.jupiter.api.Test;

class PetTests {

    @Test
    void shouldCheckCareFromUnhappy(){
       Pet pet = new Pet("Jeremy");
       pet.careChecker(); // checks if miserable, adds hungry, dirty, sad too
       List<Mood> badmoods = new ArrayList<Mood>();
       badmoods.add(Mood.DIRTY);
       badmoods.add(Mood.HUNGRY);
       badmoods.add(Mood.SAD);
       assertTrue(pet.getMood().containsAll(badmoods));
    }

    @Test
    void shouldCheckCareToUnhappy(){
        Pet pet = new Pet("Jeremy");
        pet.getMood().remove(Mood.MISERABLE);
        List<Mood> badmoods = new ArrayList<Mood>();
        badmoods.add(Mood.DIRTY);
        badmoods.add(Mood.HUNGRY);
        badmoods.add(Mood.SAD);
        pet.getMood().addAll(badmoods);
        pet.careChecker(); // checks if hungry, dirty, sad then adds miserable
        assertTrue(pet.getMood().containsAll(badmoods) && pet.getMood().contains(Mood.MISERABLE));
     }

    @Test
    void shouldFeedPet(){
       Pet pet = new Pet("Jeremy");
       pet.careChecker(); // checks if miserable, adds hungry, dirty, sad too
       pet.feed();
       assertTrue(!pet.getMood().contains(Mood.HUNGRY)); 
    }

    @Test
    void shouldSnugglePet(){
        Pet pet = new Pet("Jeremy");
        pet.careChecker(); // checks if very unhappy, adds hungry, dirty, sad too
        pet.snuggle();
        assertTrue(!pet.getMood().contains(Mood.SAD)); 
     }

     @Test
     void shouldBathePet(){
        Pet pet = new Pet("Jeremy");
        pet.careChecker(); // checks if very unhappy, adds hungry, dirty, sad too
        pet.bathe();
        assertTrue(!pet.getMood().contains(Mood.DIRTY)); 
     }

     @Test
     void shouldAgePet(){
        Pet pet = new Pet("Jeremy");
        int initialAge = pet.getAge();
        pet.bathe();
        pet.bathe();
        pet.bathe();
        pet.ageChecker();
        int newAge = pet.getAge();
        assertTrue(newAge>initialAge);
     }
   
     @Test
     void shouldSetMoodBackToHappy(){
        Pet pet = new Pet("Jeremy");
        pet.careChecker();
        pet.bathe();
        pet.snuggle();
        pet.feed();
        pet.backToHappy();
        assertTrue(pet.getMood().contains(Mood.HAPPY));
     }

    @Test
    void shouldChangeMoodToHungry(){
        Pet pet = new Pet("Jeremy");
        pet.careChecker();
        pet.feed();
        pet.setLastFed(140000L); // increasing the time of last fed to trigger the mood changer
        pet.moodChanger();
        assertTrue(pet.getMood().contains(Mood.HUNGRY)); 
    }

    @Test
    void shouldChangeMoodToSad(){
        Pet pet = new Pet("Jeremy");
        pet.careChecker();
        pet.snuggle();
        pet.setLastPet(80000L); // increasing the time of to trigger the mood changer
        pet.moodChanger();
        assertTrue(pet.getMood().contains(Mood.SAD)); 
    }

    @Test
    void shouldChangeMoodBackToDirty(){
        Pet pet = new Pet("Jeremy");
        pet.careChecker();
        pet.bathe();
        pet.setLastBathed(200000L); // increasing the time to trigger the mood changer
        pet.moodChanger();
        assertTrue(pet.getMood().contains(Mood.DIRTY)); 
    }

    @Test
    void shouldBeDead(){
        Pet pet = new Pet("Jeremy");
        pet.careChecker();
        pet.feed();
        pet.bathe();
        pet.snuggle();
        pet.setLastHappy(260000L); // increasing the time to trigger the mood changer
        pet.moodChanger();
        assertSame(State.DEAD, pet.getState()); 
    }
}


