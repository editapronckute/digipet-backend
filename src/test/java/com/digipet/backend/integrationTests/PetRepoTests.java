package com.digipet.backend.integrationTests;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import com.digipet.backend.repositories.PetRepo;
import com.digipet.backend.models.Pet;
import com.digipet.backend.models.User;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

@RunWith(SpringRunner.class)
@DataJpaTest
class PetRepoTests {

    @Autowired
    TestEntityManager entMan;

    @Autowired
    PetRepo petRepo;

    @Test
    void shouldSavePet(){
        Pet pet = new Pet("Miffy");
        petRepo.save(pet);
        assertEquals(pet.getName(), petRepo.findByName(pet.getName()).getName());
    }

    @Test 
    void shouldReturnAllPets(){
        Pet pet1 = new Pet("Hansel");
        entMan.persist(pet1);
        Pet pet2 = new Pet("Gretel");
        entMan.persist(pet2);
        Iterable<Pet> pets = petRepo.findAll();
        assertThat(pets).hasSize(2).contains(pet1, pet2);
    }

    @Test
    void shouldFindPetByOwner(){
        Pet pet = new Pet("Mario");
        User owner = new User("Momma", "123");
        pet.setOwner(owner);
        petRepo.save(pet);
        //Optional<Pet> p = Optional.of(pet);
        //Pet petfound = p.get(); // get method retrieves the object if it exist instead of Optional<object>
        assertEquals("Momma", pet.getOwner().getUsername());
    }

    @Test 
    void shouldDeletePet() {
        Pet pet1 = new Pet("Jerry");
        Pet pet2 = new Pet("Tommy");
        pet1.setId(1L);
        pet2.setId(2L);
        petRepo.save(pet1);
        petRepo.save(pet2);
        petRepo.deleteById(2L);
        assertFalse(petRepo.existsById(pet2.getId()));
    }

	@Test
    void shouldUpdatepetData(){
        Pet pet = new Pet("Jackie");
        petRepo.save(pet);
        String newName = "Joe";
        pet.setName(newName);;
        petRepo.save(pet);
        assertEquals(newName, pet.getName()); 
    }
}

 