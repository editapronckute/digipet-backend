package com.digipet.backend.integrationTests;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.Optional;

import com.digipet.backend.repositories.UserRepo;
import com.digipet.backend.models.User;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
class UserRepoTests {

    @Autowired
    TestEntityManager entMan;

    @Autowired
    UserRepo userRepo;

    @Test
    void shouldcreateUser() {
        User user = new User("John Doe", "Johny@doe.com", "Pass123");
        userRepo.save(user);
        assertEquals(user.getUsername(), userRepo.findByUsername(user.getUsername()).get().getUsername());
    }

    @Test
    void shouldReturnAllUsers() {
        User user1 = new User("Adam", "123");
        entMan.persist(user1);
        User user2 = new User("Eve", "123");
        entMan.persist(user2);
        Iterable<User> users = userRepo.findAll();
        assertThat(users).hasSize(2).contains(user1, user2);
    }

    @Test
    void shouldFindUserById(){
        User user = new User ("Mario", "SuperBros");
        user.setId(1L);
        userRepo.save(user);
        Optional<User> u = Optional.of(user);
        User userfound = u.get(); // get method retrieves the object if it exist instead of Optional<object>
        assertEquals(1L, userfound.getId());
    }

    @Test 
    void shouldDeleteUser() {
        User user1 = new User("Tom", "pass123");
        User user2 = new User("Jerry", "pass321");
        user1.setId(5L);
        user2.setId(6L);
        userRepo.save(user1);
        userRepo.save(user2);
        userRepo.deleteById(6L);
        assertFalse(userRepo.existsById(user2.getId()));
    }

    @Test
    void shouldUpdateUser() {
        User user = new User("Zelda", "princess");
        userRepo.save(user);
        String newUsername = "Xena";
        user.setUsername(newUsername);
        userRepo.save(user);
        assertThat(user.getUsername()).isEqualTo("Xena");
    }
    
}
